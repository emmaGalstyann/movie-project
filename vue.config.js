module.exports = {
    css: {
        loaderOptions: {
            sass: {
                additionalData: `
                @import "@/assets/variables.scss";
            `
            }
        }
    },
    chainWebpack: config => {
        config.module.rule('pdf')
            .test(/\.pdf$/)
            .use('file-loader').loader('file-loader')
    },
    devServer: {
        inline: true,
        port: 3001
    },
    publicPath: './',
}